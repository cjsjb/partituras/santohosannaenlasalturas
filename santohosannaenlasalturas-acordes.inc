\context ChordNames
	\chords {
	\set majorSevenSymbol = \markup { "maj7" }
	\set chordChanges = ##t

	% intro
	e2:m d2 c2 c2

	% 2,3,4,5
	e2:m d2 c2 b2:7
	e2:m d2 c2 b2:7
	e2:m d2 c2 b2:7
	e2:m d2 c2 b2:7

	% 6
	e2:m e2:m
	d2 d2
	c2 c2
	b2:7 b2:7

	% 7,8,9,10
	e2:m d2 c2 b2:7
	e2:m d2 c2 b2:7
	e2:m d2 c2 b2:7
	e2:m d2 c2 b2:7

	% 11
	e2:m e2:m
	d2 d2
	c2 c2
	b2:7 b2:7

	% 12,13,14,15
	e2:m d2 c2 b2:7
	e2:m d2 c2 b2:7
	e2:m d2 c2 b2:7
	e2:m d2 c2 b2:7

	% 16,17
	e2:m d2 c2 c2
	e2:m d2 c2 c2

	% 18
	e2:m e2:m
	}
