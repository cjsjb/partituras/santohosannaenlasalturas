\context Staff = "tenor" \with { \consists Ambitus_engraver } <<
	\set Staff.instrumentName = "Tenor"
	\set Staff.shortInstrumentName = "T."
	\set Staff.midiInstrument = "Voice Oohs"
	\set Score.skipBars = ##t
	\set Staff.printKeyCancellation = ##f
	\new Voice \global
	\new Voice \globalTempo

	\context Voice = "voz-tenor" {
		\override Voice.TextScript #'padding = #2.0
		\override MultiMeasureRest #'expand-limit = 1

		\time 2/4
		\clef "treble_8"
		\key e \minor

		R2*8  |
		e 2 (  |
%% 10
		fis 2  |
		g 2  |
		a 4 g  |
		b 4 ) e ~  |
		e 2  |
%% 15
		R2*2  |
		b, 4 ( b  |
		a 4 g  |
		a 4 g  |
%% 20
		fis 4 e  |
		g 4 ) e ~  |
		e 2  |
		R2*5  |
		r4 r8 e  |
		e 8 e 4 e 8  |
%% 30
		fis 8 fis 4 fis 8  |
		g 8 g 4 g 8  |
		a 8 a g fis  |
		b 4 e ~  |
		e 2  |
%% 35
		R2  |
		r4 r8 e  |
		e 8 e 4 e 8  |
		fis 8 fis 4 fis 8  |
		g 8 g 4 g 8  |
%% 40
		a 8 a g fis  |
		b 4 e ~  |
		e 2  |
		R2*2  |
%% 45
		r4 b  |
		b 4 a 8 g  |
		a 2  |
		d 4
		% warning: bar too short, padding with rests
		% 90240 + 960 < 92160  &&  1/4 < 2/4
		r4  |
		R2*3  |
		r4 r8 e  |
		e 8 e 4 e 8  |
		fis 8 fis 4 fis 8  |
%% 55
		g 8 g 4 g 8  |
		a 8 a g fis  |
		b 4 e ~  |
		e 2  |
		R2  |
%% 60
		r4 r8 e  |
		e 8 e 4 e 8  |
		fis 8 fis 4 fis 8  |
		g 8 g 4 g 8  |
		a 8 a g fis  |
%% 65
		b 4 e ~  |
		e 2  |
		R2  |
		r4 r8 e  |
		e 8 e 4 e 8  |
%% 70
		fis 8 fis 4 fis 8  |
		c' 8 c' 4. ~  |
		c' 4 r8 e  |
		e 8 e 4 e 8  |
		fis 8 fis 4 fis 8  |
%% 75
		c' 8 c' 4. ( ~  |
		c' 2  |
		b 2 ~  |
		b 2 )  |
		\bar "|."
	}

	\new Lyrics \lyricsto "voz-tenor" {
		San -- to. __
		San -- to. __

		Ho -- san -- na, ho -- san -- na,
		ho -- san -- na, ho -- san -- "na en" las al -- tu -- ras. __

		Ho -- san -- na, ho -- san -- na,
		ho -- san -- na, ho -- san -- "na en" las al -- tu -- ras. __

		Ben -- di -- "to el" que vie -- ne...
		%en el nom -- bre
		%del Se -- ñor. __

		Ho -- san -- na, ho -- san -- na,
		ho -- san -- na, ho -- san -- "na en" las al -- tu -- ras. __

		Ho -- san -- na, ho -- san -- na,
		ho -- san -- na, ho -- san -- "na en" las al -- tu -- ras. __

		¡Ho -- san -- na, ho -- san -- na, ho -- san -- na! __
		¡Ho -- san -- na, ho -- san -- na, ho -- san -- na! __
	}
>>

