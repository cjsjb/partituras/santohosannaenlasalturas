\context Staff = "soprano" \with { \consists Ambitus_engraver } <<
	\set Staff.instrumentName = "Soprano"
	\set Staff.shortInstrumentName = "S."
	\set Staff.midiInstrument = "Voice Oohs"
	\set Score.skipBars = ##t
	\set Staff.printKeyCancellation = ##f
	\new Voice \global
	\new Voice \globalTempo

	\context Voice = "voz-soprano" {
		\override Voice.TextScript #'padding = #2.0
		\override MultiMeasureRest #'expand-limit = 1

		\time 2/4
		\clef "treble"
		\key e \minor

		R2*4  |
%% 5
		e' 2 (  |
		fis' 2  |
		g' 2  |
		a' 4 g'  |
		b' 4 ) e' ~  |
%% 10
		e' 2  |
		R2*2  |
		b 4 ( b'  |
		a' 4 g'  |
%% 15
		a' 4 g'  |
		fis' 4 e'  |
		g' 4 ) e' ~  |
		e' 2  |
		R2*2  |
		r4 b'  |
		b' 4 a' 8 g'  |
		a' 2  |
		d' 4 d' 8 d'  |
%% 25
		g' 4 g'  |
		\times 2/3 { r4 fis' e' }  |
		fis' 2 (  |
		e' 4 dis' )  |
		e' 2 ~  |
%% 30
		e' 2  |
		R2*2  |
		b 4 ( b'  |
		a' 4 g'  |
%% 35
		a' 4 g'  |
		fis' 4 e'  |
		g' 4 ) e' ~  |
		e' 2  |
		R2*2  |
		b 4 ( b'  |
		a' 4 g'  |
		a' 4 g'  |
		fis' 4 e'  |
%% 45
		g' 4 ) e' ~  |
		e' 2  |
		R2  |
		r4 d' 8 d'  |
		g' 4 g'  |
%% 50
		\times 2/3 { r4 fis' e' }  |
		fis' 2 (  |
		e' 4 dis'  |
		e' 2 ~  |
		e' 2 )  |
%% 55
		R2*2  |
		b 4 ( b'  |
		a' 4 g'  |
		a' 4 g'  |
%% 60
		fis' 4 e'  |
		g' 4 ) e' ~  |
		e' 2  |
		R2*2  |
%% 65
		b 4 ( b'  |
		a' 4 g'  |
		a' 4 g'  |
		fis' 4 e'  |
		g' 4 ) e' ~  |
%% 70
		e' 4. fis' 8  |
		c'' 8 c'' 4. ~  |
		c'' 4 r8 e'  |
		e' 8 e' 4 e' 8  |
		fis' 8 fis' 4 fis' 8  |
%% 75
		c'' 8 c'' 4. ( ~  |
		c'' 2  |
		b' 2 ~  |
		b' 2 )  |
		\bar "|."
	}

	\new Lyrics \lyricsto "voz-soprano" {
		San -- to. __
		San -- to. __

		El cielo y la tie -- rra
		es -- tán lle -- nos
		de tu glo -- ria. __

		San -- to. __
		San -- to. __

		%Ben -- di -- "to el" que vie -- ne
		"...en" el nom -- bre
		del Se -- ñor. __

		San -- to. __
		San -- to. __

		¡Ho -- san -- na! __
		¡Ho -- san -- na, ho -- san -- na, ho -- san -- na! __
	}
>>
