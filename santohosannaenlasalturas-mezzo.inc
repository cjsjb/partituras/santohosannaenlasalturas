\context Staff = "mezzo" \with { \consists Ambitus_engraver } <<
	\set Staff.instrumentName = "Mezzosoprano"
	\set Staff.shortInstrumentName = "M."
	\set Staff.midiInstrument = "Voice Oohs"
	\set Score.skipBars = ##t
	\set Staff.printKeyCancellation = ##f
	\new Voice \global
	\new Voice \globalTempo

	\context Voice = "voz-mezzo" {
		\override Voice.TextScript #'padding = #2.0
		\override MultiMeasureRest #'expand-limit = 1

		\time 2/4
		\clef "treble"
		\key e \minor

		R2*4  |
%% 5
		e' 2 (  |
		d' 2  |
		c' 2  |
		b 2 ~  |
		b 4 ) b ~  |
%% 10
		b 2  |
		R2*2  |
		e' 2 (  |
		d' 2  |
%% 15
		c' 2  |
		b 2 ~  |
		b 4 ) b ~  |
		b 2  |
		R2*2  |
		r4 g'  |
		g' 4 fis' 8 e'  |
		fis' 2  |
		fis' 4 d' 8 d'  |
%% 25
		g' 4 g'  |
		\times 2/3 { r4 a' g' }  |
		a' 2 (  |
		g' 4 fis' )  |
		g' 2 ~  |
%% 30
		g' 2  |
		R2*2  |
		e' 2 (  |
		d' 2  |
%% 35
		c' 2  |
		b 2 ~  |
		b 4 ) b ~  |
		b 2  |
		R2*2  |
		e' 2 (  |
		d' 2  |
		c' 2  |
		b 2 ~  |
%% 45
		b 4 ) b ~  |
		b 2  |
		R2  |
		r4 d' 8 d'  |
		g' 4 g'  |
%% 50
		\times 2/3 { r4 a' g' }  |
		a' 2 (  |
		g' 4 fis'  |
		g' 2 ~  |
		g' 2 )  |
%% 55
		R2*2  |
		e' 2 (  |
		d' 2  |
		c' 2  |
%% 60
		b 2 ~  |
		b 4 ) b ~  |
		b 2  |
		R2*2  |
%% 65
		e' 2 (  |
		d' 2  |
		c' 2  |
		b 2 ~  |
		b 4 ) b ~  |
%% 70
		b 4. fis' 8  |
		g' 8 g' 4. ~  |
		g' 4 r8 e'  |
		e' 8 e' 4 e' 8  |
		fis' 8 fis' 4 fis' 8  |
%% 75
		g' 8 g' 4. ~  |
		g' 2 ~  |
		g' 2 ~  |
		g' 2  |
		\bar "|."
	}

	\new Lyrics \lyricsto "voz-mezzo" {
		San -- to. __
		San -- to. __

		El cielo y la tie -- rra
		es -- tán lle -- nos
		de tu glo -- ria. __

		San -- to. __
		San -- to. __

		%Ben -- di -- "to el" que vie -- ne
		"...en" el nom -- bre
		del Se -- ñor. __

		San -- to. __
		San -- to. __

		¡Ho -- san -- na! __
		¡Ho -- san -- na, ho -- san -- na, ho -- san -- na! __
	}
>>
